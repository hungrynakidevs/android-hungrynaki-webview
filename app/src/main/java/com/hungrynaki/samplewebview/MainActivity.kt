package com.hungrynaki.samplewebview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpWebView()
        loadHungryNaki()
    }

    private fun loadHungryNaki() {
        webviewKitchen.loadUrl("https://v4-staging.hungrynaki.com/", getHeaders())
    }

    fun getHeaders(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        headers["X-TOKEN"] = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodW5ncnluYWtpLmNvbSIsImRhdGEiOnsidXVpZCI6ImJiZWMxNzQ2LWE3MGYtNDgwYS04OTI4LTM2ODI4MGZmYTlhZSIsInJvbGUiOiJjdXN0b21lciIsImN1c3RvbWVySWQiOiI1Y2I4NWE0MmQ1NWFkNTIyMzdkODA2ZTAifSwiaWF0IjoxNTc2NjY0NzgzLCJleHAiOjE1ODQ0NDA3ODN9.KisA5YowVdPvt08haHiiWqENG_FVD7TrppuJ5m1HFZ9Znb1N9-suJidDeqpnTP35fbnMa26XjLl7wOOwDHio8rt5TbYh12jWl9k9n58G6-v9vypZEczHj6Yqrfp0YldvbFWXdARz7eBz3nggamNeCBbpKMiss8WGu_zorofqwci7t1bULNjJsH3hRzk9ubz81IDGL-kRIvWjC9epLFWLC0LeUD-JmIerXN9Z-vdkFKmkL-4qEd0hqVmeokBSg2MIkautRhKGjWN0Qo3gdsZQzPMQPjgvyJxl2cSAQ_0IWvkPl5bTaPYibxDeTcj_4rWKg4-u6WyVwEugs8befZ9agoM4aoKiqKINyXUkX8DoLkaJmEqUoDz9nI-xTbpyLdRTmHmnPH_UwWUm0MZWPCGlpDMRqikgV5C6V8djlDfEHSgCrl7qdNQc38kcBOOs_3GfmMj3y1M2Fqug9RJy9vS1OoBCUocrdcmP5UiX9evcc2PjJVESZNu8Pj31ujY35kizB8u6yFsU3tD1KeMy_q3cwrLirUWDnFG4y5sBC_AEOvKqunqowsE89kVs5pwXiEbbqw4dcU98qOUzo4ieuxiEGAY6rGXhbVBV8iPwq9BM0jaWGFt1V5VjJn3PRQuWFQuTrys8ajnQYzAW_LC2cq7vzUqNoC-zFsAWQOFwYY7clDA"
        headers["X-REFRESH-TOKEN"] = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodW5ncnluYWtpLmNvbSIsImRhdGEiOnsiZmluZ2VyUHJpbnQiOiIkMmIkMTAkN2F2YkE0RGlFNkRVSlRFa2tCaWk5ZUFWQjVhU01rbHhIT2ZqcEZaTmRUNEMxQ1JqYWRzUTYifSwiaWF0IjoxNTc2NDI0ODcxLCJleHAiOjE1ODQyMDA4NzF9.Efhy8qTWuK7tmy2WnAms4dpqhWHCPvKn3qEb92iXGQpnQdNVthDS9lAY0JrVDW_tCPwVUTSnImOkuik4LqlP84n9x_kWEgbn5SToHuy7n6q-aCdEhjSaU4VMkQe95w9HJKyANjT78s9yhma1OxO7gjrArrwTbmfjaAsKHCOdPcHtKTZb2gnVbbEu8af5MORN7mX1-QzxKu2jdCpJHItpZwcBF4hiq13mnkMlqRidtC8HEIVuRw8sIBr2dBJE5OWpWiW0JF89iWk_8J6xZGTAJyvMjHGd-K44_H9IKZwr0LQB-Ws3rY-m3FUMynZSnvMpErSW-TSqOe0P7Agj8z8EhNliiThWGqxl0XJ2hmoSvxC3UA6l_LzccQnZlDeCdDc66SOYPiVq69Og84kt9lDDHXkeAtfuxl7fTjlrcBP_bUfwOrWoLjgrulJzZiKibvEpYi7hOD6B4YyhJZr9mCGTnDGLDNCSSt1JM-SCz6CywseDjwBFB9G-ep0gPT_5nP5Ox-beVpZO7QqA6Ue81T2aU2d0xLyY4cXmDiDmvOEJ3vkpig6Bwu4pknWNa9K6htWuMzxLe4l62ti_ViXf5_QZVz7RsqTmdjCa9WMgvP1W5zNnmHjmsE3i3ltzTs3_acbjXsNrRUQLlXzSGrUYNlIUvt1JNccaDqhrxPEpJdty104"
        headers["X-LAT"] = "23.782242"
        headers["X-LNG"] = "90.3951344"
        return headers
    }

    private fun setUpWebView() {
        webviewKitchen.settings.javaScriptEnabled = true
        webviewKitchen.settings.loadsImagesAutomatically = true
        webviewKitchen.webViewClient = MyWebViewClient
        webviewKitchen.clearHistory()
        webviewKitchen.clearFormData()
        webviewKitchen.clearCache(true)
        webviewKitchen.settings.domStorageEnabled = true
        webviewKitchen.settings.cacheMode = WebSettings.LOAD_NO_CACHE
    }


    private val MyWebViewClient = object : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            webviewKitchen.loadUrl(url)
            return true
        }
    }
}
